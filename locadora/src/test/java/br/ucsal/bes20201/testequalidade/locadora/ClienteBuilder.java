
public class ClienteBuilder {
	
	private static final String cpfDF = "85781160540";

	private static final String nomeDF = "Samuel Jesus";

	private static final String telefoneDF = "71991531003";

	private String cpf = cpfDF;

	private String nome = nomeDF;

	private String telefone = telefoneDF;

	public  ClienteBuilder() {

	}

	public static ClienteBuilder umCliente() {
		return new ClienteBuilder(); 

	}

	public  ClienteBuilder comCpf(String cpf) {
		this.cpf = cpf;
		return this;

	}

	public ClienteBuilder comNome(String nome) {
		this.nome = nome;
		return this;

	}

	public ClienteBuilder comTelefone(String telefone) {
		this.telefone = telefone;
		return this;

	}

	public ClienteBuilder mas() {
		return umCliente().comCpf(cpf).comNome(nome).comTelefone(telefone);

	}

	public Cliente builder() {
		Cliente cliente = new Cliente(cpf, nome, telefone);
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setTelefone(telefone);
		return cliente;

	}

}
