
public class VeiculoBuilder {
	
	private static final String placaDF = "OKZ1378";

	private static final Integer anoDF = 1;

	private static final ModeloBuilder modeloDF = ModeloBuilder.umModelo();

	private static final Double valorDiariaDF = 2D;

	private static final SituacaoVeiculoEnum situacaoDF = SituacaoVeiculoEnum.DISPONIVEL;

	private String placa = placaDF;

	private Integer ano = anoDF;

	private ModeloBuilder modelo = modeloDF;

	private Double valorDiaria = valorDiariaDF;

	private SituacaoVeiculoEnum situacao = situacaoDF;

	public VeiculoBuilder() {
	}

	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}

	public String getPlaca() {
		return placa;
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;

	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;

	}

	public VeiculoBuilder comModelo(ModeloBuilder modelo) {
		this.modelo = modelo.mas();
		return this;

	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;

	}

	public VeiculoBuilder comSituacaoDisponivel(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.DISPONIVEL;
		return this;

	}

	public VeiculoBuilder comSituacaoLocado(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.LOCADO;
		return this;

	}

	public VeiculoBuilder comSituacaoManutencao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.MANUTENCAO;
		return this;

	}

	public VeiculoBuilder mas() {
		return umVeiculo().comAno(ano).comModelo(modelo).comPlaca(placa).comSituacaoDisponivel(situacao);
	}

	public Veiculo Builder() {
		Veiculo veiculo = new Veiculo(placa, ano, null, valorDiaria);
		veiculo.setAno(ano);
		veiculo.setPlaca(placa);
		veiculo.setSituacao(situacao);
		veiculo.setValorDiaria(valorDiaria);
		return veiculo;
	}

}
