
public class ModeloBuilder {
	
	private static final Integer seqDF = 0;

	private static final Integer codigoDF = 1;

	private static final String nomeDF = "OP";

	private Integer seq = seqDF;

	private Integer codigo = codigoDF;

	private String nome = nomeDF;

	public ModeloBuilder() {

	}

	public static ModeloBuilder umModelo() {
		return new ModeloBuilder();

	}

	public ModeloBuilder comNome(String nome) {
		this.nome = nome;
		return this;

	}

	public ModeloBuilder comSeq(Integer seq) {
		this.seq = seq;
		return this;

	}

	public ModeloBuilder comCodigo(Integer codigo) {
		this.codigo = codigo;
		return this;

	}

	public ModeloBuilder mas() {
		return umModelo().comNome(nome).comCodigo(codigo).comSeq(seq);
	}

	public Modelo Builder() {
		Modelo modelo = new Modelo(nome);
		modelo.setNome(nome);
		return modelo;
	}

}
